const User = require("../models/user");
const Product = require("../models/product")
const bcrypt = require("bcrypt")
const auth = require("../auth")

module.exports.checkDuplicate = (body) => {
	return User.find({
		email: body.email,
		username: body.username
	}).then(result => {
		if(result.length > 0){
			return true; // true meaning a duplicate email exists
		}else{
			return false; // no duplicate email found
		}
	})
}


//Register User
module.exports.registerUser = (body) => {
	let newUser = new User({
		username: body.username,
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		password: bcrypt.hashSync(body.password, 10),
		mobileNumber: body.mobileNumber,
		gender: body.gender,
		dob: body.dob
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

//Authenticate user
module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())}
			}else{
				return false;
			}
		}
	})
}

//Update User
module.exports.updateUser = (params, body) => {
	let updatedUser = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(params.userId, updatedUser).then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

// //Apply admin user
// module.exports.updateUser = (params, body) => {
// 	let updatedUser = {
// 		isAdmin: true
// 	}

// 	return User.findByIdAndUpdate(params.userId, updatedUser).then((user, error) => {
// 		if(error){
// 			return false;
// 		}else{
// 			return true;
// 		}
// 	})
// }

//Get user details
module.exports.getProfile = (userId) => {
	console.log(userId)
	return User.findById(userId).then(result => {
		result.password = undefined;
		console.log(result)
		return result;
	})
}

//User checkout
// module.exports.cart = async (data) => {
// 	let cartSaveStatus = await Product.findById(data.productId).then(product => {
// 		console.log(product)
// 		product.buyer.push({userId: data.userId})
// 		return product.save().then((product, err) => {
// 			if(err){
// 				return false; //user not added to enrollees
// 			}else{
// 				return true; // user successfully added to enrollees
// 			}
// 		})
// 	})

// 	let userSaveStatus = await User.findById(data.userId).then(user => {
// 		user.myOrder.push({productId: data.productId, quantity: data.quantity, total: data.total})
// 		return user.save().then((user, err) => {
// 			if(err){
// 				return false; // course not save in user's enrollments
// 			}else{
// 				return true; // course successfully save in user's enrollments
// 			}
// 		})
// 	})

// 	if(cartSaveStatus && userSaveStatus){
// 		return true;
// 	}else{
// 		return false;
// 	}
// }

module.exports.checkout = (userId, cart) => {
	return User.findById(userId).then(user => {
		if(user === null){
			return false;
		} else {
			user.orders.push(
				{
					products: cart.products,
					totalAmount: cart.totalAmount
				}
			);
			return user.save().then((updatedUser, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			})
		}
	})
}

module.exports.getMyCart = (userId) => {
	console.log(userId)
	return User.findById(userId).then(result => {
		console.log(result)
		result.password = undefined;
		return result.orders;
	})
}

module.exports.allOrder = () => {
	return User.find({orders: { $exists: true, $not: {$size: 0} } }).then(result => {
		console.log(result)
		return result;
	})
}