const Product = require("../models/product");
const bcrypt = require("bcrypt")
const auth = require("../auth")

//Create a product - Admin
module.exports.addProduct = (body) => {

	let newProduct = new Product({
		name: body.name,
		description: body.description,
		rating: body.rating,
		amount: body.amount,
		color: body.color,
		category: body.category
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false; //save unsuccessful
		}else{
			return true; //save successful
		}
	})

}

//Retrieve all active products
module.exports.allProduct = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

//Retrieve all computer products
module.exports.computerProduct = () => {
	return Product.find({category: 'Computer'}).then(result => {
		return result;
	})
}

//Retrieve all phone products
module.exports.phoneProduct = () => {
	return Product.find({category: 'Phone'}).then(result => {
		return result;
	})
}

//Retrieve all console products
module.exports.consoleProduct = () => {
	return Product.find({category: 'Console'}).then(result => {
		return result;
	})
}

//Retrieve single product
module.exports.getProduct = (params) => {
	console.log(params)
	return Product.findById(params.productId).then(result => {
		return result;
	})
}

//Update a product - Admin
module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		rating: body.rating,
		amount: body.amount,
		color: body.color,
		category: body.category
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

//Archive a product - Admin
module.exports.archiveProduct = (params) => {
	let updatedProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})  
}