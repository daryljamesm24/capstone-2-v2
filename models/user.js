const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "Username is required"]
	},
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	gender: {
		type: String,
		required: [true, "Gender is required"]
	},
	dob: {
		type: String,
		required: [true, "Date of birth is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders : [ 
	{
		products : [
			{
				productId : {
					type: String,
					required : [true, "Product ID is required"]
				},
				name: {
					type: String,
					required : [true, "Name is required"]
				},
				amount: {
					type: Number,
					required: [true, "Amount is required"]
				},
				quantity : {
					type: Number,
					required : [true, "Product quantity is required"]
				}
			}
		],
		totalAmount : {
			type: Number,
			required : [true, "Total amount is required"]
		},
		purchasedOn : {
			type : Date,
			default : new Date()
		}
	}
		]
})

module.exports = mongoose.model("User", userSchema);