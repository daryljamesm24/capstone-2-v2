const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	rating: {
		type: Number,
		required: [true, "Rating is required"]
	},
	amount: {
		type: Number,
		required: [true, "Amount is required"]
	},
	color: {
		type: String,
		required: [true, "Color is required"]
	},
	category: {
		type: String,
		enum: ['Computer', 'Phone', 'Console'],
		message: '{VALUE} is not supported'
	},
	isActive: {
		type: Boolean,
		default: true
	},
	addedOn: {
		type: Date,
		default: new Date()
	},
	buyer: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);