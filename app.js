const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
// insert const productRoutes = require("./routes/productRoutes"); here
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")
// const orderRoutes = require("./routes/orderRoutes")

const app = express();
const port = 5000

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({
	extended: true
}));

// insert app.use("/products", productRoutes) here
app.use("/users", userRoutes)
app.use("/products", productRoutes)
// app.use("/orders", orderRoutes)

mongoose.connect("mongodb+srv://admin:admin123@cluster0.uabj3.mongodb.net/b110_ecommerce_v2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => {
	console.log("Now connected to MongoDB Atlas.")
})

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${
		process.env.PORT || port
	}`)
})