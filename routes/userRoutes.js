const express = require("express");
const router = express.Router();
const auth = require("../auth")
const userController = require("../controllers/userController")

// Route to check for duplicate emails
router.post("/checkDuplicate", (req, res) => {
	userController.checkDuplicate(req.body).then(resultFromDuplicateExists => res.send(resultFromDuplicateExists))
})

//Register user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

//Authenticate user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//Update user
router.put("/:userId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	
	if(token.isAdmin === true){
		userController.updateUser(req.params, req.body).then(resultFromUpdate => res.send(resultFromUpdate))
	}else{
		res.send({auth: "Not an admin"})
	}
})

// //Apply admin user
// router.put("/:userId", auth.verify, (req, res) => {
// 	let token = auth.decode(req.headers.authorization)
	
// 	if(token.isAdmin === false){
// 		userController.updateUser(req.params, req.body).then(resultFromUpdate => res.send(resultFromUpdate))
// 	}else{
// 		res.send({auth: "Already an admin"})
// 	}
// })

//Get user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	userController.getProfile(userData.id).then(resultFromGetProfile => res.send(resultFromGetProfile))
})

// User checkout
// router.post("/cart", auth.verify, (req, res) => {
// 	const data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		productId: req.body.productId,
// 		quantity: req.body.quantity,
// 		total: req.body.total
// 	}
	
// 	userController.cart(data).then(resultFromCart => res.send(resultFromCart))
// })
router.post("/checkout", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	userController.checkout(userId, req.body).then(resultFromController => res.send(resultFromController));
})

// Get My Order
router.get("/myCart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	userController.getMyCart(userData.id).then(resultFromGetProfile => res.send(resultFromGetProfile))
})

// Get All Order - admin only
router.get("/allOrder", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){
	userController.allOrder().then(resultFromGetAllOrder => res.send(resultFromGetAllOrder))
	}else{
		res.send({auth: "Not an admin"})
	}
})

module.exports = router