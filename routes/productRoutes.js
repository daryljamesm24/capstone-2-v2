const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController")
const auth = require("../auth")

//Create a product - Admin
router.post("/create", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){
		productController.addProduct(req.body).then(resultFromAddProduct => res.send(resultFromAddProduct))
	}else{
		res.send({auth: "Not an admin"})
	}
	
})

//Retrieve all active products
router.get("/all", (req, res) => {
	productController.allProduct().then(resultFromAll => res.send(resultFromAll))
})

//Retrieve all computer products
router.get("/computers", (req, res) => {
	productController.computerProduct().then(resultFromComputerProduct => res.send(resultFromComputerProduct))
})

//Retrieve all phone products
router.get("/phones", (req, res) => {
	productController.phoneProduct().then(resultFromPhoneProduct => res.send(resultFromPhoneProduct))
})

//Retrieve all console products
router.get("/consoles", (req, res) => {
	productController.consoleProduct().then(resultFromConsoleProduct => res.send(resultFromConsoleProduct))
})

//Retrieve single product
router.get("/:productId", (req, res) => {
	console.log(req.params)
	productController.getProduct(req.params).then(resultFromGetSpecific => res.send(resultFromGetSpecific))
})

//Update a product - Admin
router.put("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){

	productController.updateProduct(req.params, req.body).then(resultFromUpdate => res.send(resultFromUpdate))
	}else{
		res.send({auth: "Not an admin"})
	}
})

//Archive Product - Admin
router.delete("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){

	productController.archiveProduct(req.params).then(resultFromArchive => res.send(resultFromArchive))
	}else{
		res.send({auth: "Not an admin"})
	}
})

module.exports = router